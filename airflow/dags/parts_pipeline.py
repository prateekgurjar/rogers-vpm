from datetime import datetime, timedelta
from airflow import DAG, AirflowException
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from tasks.s3toRedshift import dump_to_redshift

default_args =  {
    "owner": "airflow",
    "start_date": '2019-08-14',
    "email_on_failure": False,
    "email_on_retry": False,
    "retries": 0,
    "retry_delay": timedelta(minutes=5),
}

dag = DAG('Parts', description='Data Pipeline to extract data from Parts, perform transforms and dumps into data warehouse',
          schedule_interval='*/5 * * * *', catchup=False,
          default_args = default_args)

dummy_operator = DummyOperator(task_id='Pipeline_Initializer', retries=3, dag=dag)

parts_to_redshift = PythonOperator(task_id='parts_to_redshift', provide_context=True, python_callable=dump_to_redshift, dag=dag)

dummy_operator >> parts_to_redshift