#
# Prerequisites
# In order to run this script, you will require:
#  - a VPC with 2 subnets
#  - 2 secrets in AWS Secrets Manager
#     - AirflowFernetKey
#     - AirflowFlowerauth
#  - see inline param documenation for more detials
AWSTemplateFormatVersion: 2010-09-09
Description: ECS Cluster to run Airflow.
Parameters:
  VPC:
    Type: 'AWS::EC2::VPC::Id'
  SubnetA:
    Type: 'AWS::EC2::Subnet::Id'
  SubnetB:
    Type: 'AWS::EC2::Subnet::Id'
  Image:
    Type: String
    Default: 843706529727.dkr.ecr.ca-central-1.amazonaws.com/vpm/airflow
  AppName:
    Type: String
    Default: VPM-Airflow
  LogsPrefix:
    Type: String
    Default: /vpm/ecs/airflow
  DBClass:
    Default: db.t2.micro
    Description: Database instance class
    Type: String
    AllowedValues:
      - db.t2.micro
      - db.t2.small
      - db.t2.medium
      - db.m1.large
      - db.m1.xlarge
      - db.m2.xlarge
    ConstraintDescription: must select a valid database instance type.
  DBAllocatedStorage:
    Default: '20'
    Description: The size of the database (Gb)
    Type: Number
    MinValue: '5'
    MaxValue: '6144'
    ConstraintDescription: must be between 5+
  AirflowFernetKeySecretName:
    Default: AirflowFernetKey
    Description: Name of Secret in AWS Secrets Manager - should have a single key with the name 'key'
    Type: String
  AirflowFlowerAuthSecretName:
    Default: AirflowFlowerAuth
    Description: Name of secret in AWS Secrets Manager - shoudl have a single key with the name 'basicauthstring' and be in the format 'user1:password1,user2:password2'
    Type: String
  Certificate:
    Default: arn:aws:acm:region:123456789012:certificate/00000000-0000-0000-0000-000000000000
    Description: ARN of the SSL Certificate to use for this load balancer
    Type: String
  ContainerTag:
    Description: The tag of the container in ECR
    Type: String
  Stage:
    Type: String
    Default: dev
  BaseUrl:
    Type: String
    Default: http://rogers-vpm-airflow-dev.massiveinsights.com

Resources:


#############################################################################
#
# CLUSTER
#
#############################################################################
  Cluster:
    Type: 'AWS::ECS::Cluster'
    Properties:
      ClusterName: !Join 
        - ''
        - - !Ref AppName
          - Cluster

#############################################################################
#
# ROLES
#
#############################################################################
  ExecutionRole:
    Type: 'AWS::IAM::Role'
    Properties:
      RoleName: !Join 
        - ''
        - - !Ref AppName
          - ExecutionRole
      AssumeRolePolicyDocument:
        Statement:
          - Effect: Allow
            Principal:
              Service: ecs-tasks.amazonaws.com
            Action: 'sts:AssumeRole'
      ManagedPolicyArns:
        - 'arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy'

  TaskRole:
    Type: 'AWS::IAM::Role'
    Properties:
      RoleName: !Join 
        - ''
        - - !Ref AppName
          - TaskRole
      AssumeRolePolicyDocument:
        Statement:
          - Effect: Allow
            Principal:
              Service: ecs-tasks.amazonaws.com
            Action: 'sts:AssumeRole'
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/AmazonS3FullAccess


  #######################################################################
  #
  # Security Groups
  #
  #######################################################################

  ContainerSecurityGroup:
    Type: 'AWS::EC2::SecurityGroup'
    Properties:
      GroupDescription: !Join 
        - ''
        - - !Ref AppName
          - ContainerSecurityGroup
      VpcId: !Ref VPC
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: 8080
          ToPort: 8080
          SourceSecurityGroupId: !Ref  LoadBalancerSecurityGroup
          #CidrIp: 0.0.0.0/0
        - IpProtocol: tcp
          FromPort: 6379
          ToPort: 6379
          CidrIp: 0.0.0.0/0
        - IpProtocol: tcp
          FromPort: 5555 
          ToPort: 5555 
          CidrIp: 0.0.0.0/0

  LoadBalancerSecurityGroup:
    Type: 'AWS::EC2::SecurityGroup'
    Properties:
      GroupDescription: VPMWebLoadBalancerSecurityGroup
      VpcId: !Ref VPC
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: 8080
          ToPort: 8080
          CidrIp: 0.0.0.0/0
        - IpProtocol: tcp
          FromPort: 80
          ToPort: 80
          CidrIp: 0.0.0.0/0
        - IpProtocol: tcp
          FromPort: 443
          ToPort: 443
          CidrIp: 0.0.0.0/0

  DBSecurityGroup:
    Type: 'AWS::EC2::SecurityGroup'
    Properties:
      GroupDescription: DB Security Group 
      VpcId: !Ref VPC 
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: 5432
          ToPort: 5432
          CidrIp: 10.0.0.0/16

  #############################################################################
  #
  # Task Definitions
  #
  #############################################################################
  WebTaskDefinition:
    Type: 'AWS::ECS::TaskDefinition'
    Properties:
      Family: !Join 
        - '-'
        - - !Ref AppName
          - web-taskdef
      NetworkMode: awsvpc
      RequiresCompatibilities:
        - FARGATE
      Cpu: 512
      Memory: 1.0GB
      ExecutionRoleArn: !Ref ExecutionRole
      TaskRoleArn: !Ref TaskRole
      ContainerDefinitions:
        - Name: web-cont
          Image: !Join
            - ':'
            - - !Ref Image
              - !Ref ContainerTag
          EntryPoint: 
            - /entrypoint.sh
          Command: 
            - webserver
          WorkingDirectory: /usr/local/airflow
          PortMappings:
            - ContainerPort: 8080
          LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-region: !Ref 'AWS::Region'
              awslogs-group: !Ref LogGroup
              awslogs-stream-prefix: ecs
          Environment:
            - Name: LOAD_EX
              Value: n
            - Name: POSTGRES_HOST 
              Value: !GetAtt pgDB.Endpoint.Address 
            - Name: POSTGRES_USER
              Value: !Join ['', ['{{resolve:secretsmanager:', !Ref MyRDSInstanceSecret, ':SecretString:username}}' ]]
            - Name: POSTGRES_PASSWORD
              Value: !Join ['', ['{{resolve:secretsmanager:', !Ref MyRDSInstanceSecret, ':SecretString:password}}' ]]
            - Name: POSTGRES_DB
              Value: airflow
            - Name: FERNET_KEY
              Value: !Join ['', ['{{resolve:secretsmanager:', !Ref AirflowFernetKeySecretName, ':SecretString:key}}' ]]
            - Name: EXECUTOR
              Value: Celery
            - Name: REDIS_HOST 
              Value: redis.ecslocal
            - Name: AIRFLOW__CORE__REMOTE_BASE_LOG_FOLDER
              Value: !Join ['.',['s3://ca.rogers.vpm', !Ref Stage , 'logs/airflow/logs']]
            - Name: AIRFLOW__CORE__REMOTE_LOGGING
              Value: True
            - Name: AIRFLOW__CORE__REMOTE_LOG_CONN_ID
              Value: aws_default
            - Name: AIRFLOW__WEBSERVER__BASE_URL
              Value: !Join ['',['https://rogers-vpm-airflow-', !Ref Stage, '.massiveinsights.com']]


  FlowerTaskDefinition:
    Type: 'AWS::ECS::TaskDefinition'
    Properties:
      Family: !Join 
        - '-'
        - - !Ref AppName
          - flower-taskdef
      NetworkMode: awsvpc
      RequiresCompatibilities:
        - FARGATE
      Cpu: 256
      Memory: 0.5GB
      ExecutionRoleArn: !Ref ExecutionRole
      TaskRoleArn: !Ref TaskRole
      ContainerDefinitions:
        - Name: flower-cont
          Image: !Join
            - ':'
            - - !Ref Image
              - !Ref ContainerTag
          EntryPoint: 
            - /entrypoint.sh
          Command: 
            - flower
          WorkingDirectory: /usr/local/airflow
          PortMappings:
            - ContainerPort: 5555
          LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-region: !Ref 'AWS::Region'
              awslogs-group: !Ref LogGroup
              awslogs-stream-prefix: ecs
          Environment:
            - Name: LOAD_EX
              Value: n
            - Name: POSTGRES_HOST 
              Value: !GetAtt pgDB.Endpoint.Address 
            - Name: POSTGRES_USER
              Value: !Join ['', ['{{resolve:secretsmanager:', !Ref MyRDSInstanceSecret, ':SecretString:username}}' ]]
            - Name: POSTGRES_PASSWORD
              Value: !Join ['', ['{{resolve:secretsmanager:', !Ref MyRDSInstanceSecret, ':SecretString:password}}' ]]
            - Name: POSTGRES_DB
              Value: airflow
            - Name: FERNET_KEY
              Value: !Join ['', ['{{resolve:secretsmanager:', !Ref AirflowFernetKeySecretName, ':SecretString:key}}' ]]
            - Name: EXECUTOR
              Value: Celery
            - Name: REDIS_HOST 
              Value: redis.ecslocal 
            - Name: AIRFLOW__CELERY__FLOWER_BASIC_AUTH
              Value: !Join ['', ['{{resolve:secretsmanager:', !Ref AirflowFlowerAuthSecretName, ':SecretString:basicauthstring}}' ]]
            - Name: AIRFLOW__CORE__REMOTE_BASE_LOG_FOLDER
              Value: !Join ['.',['s3://ca.rogers.vpm', !Ref Stage , 'logs/airflow/logs']]
            - Name: AIRFLOW__CORE__REMOTE_LOGGING
              Value: True
            - Name: AIRFLOW__CORE__REMOTE_LOG_CONN_ID
              Value: aws_default
            - Name: AIRFLOW__WEBSERVER__BASE_URL
              Value: !Join ['',['https://rogers-vpm-airflow-', !Ref Stage, '.massiveinsights.com']]

  SchedulerTaskDefinition:
    Type: 'AWS::ECS::TaskDefinition'
    Properties:
      Family: !Join 
        - '-'
        - - !Ref AppName
          - scheduler-taskdef
      NetworkMode: awsvpc
      RequiresCompatibilities:
        - FARGATE
      Cpu: 256
      Memory: 0.5GB
      ExecutionRoleArn: !Ref ExecutionRole
      TaskRoleArn: !Ref TaskRole
      ContainerDefinitions:
        - Name: sched-cont
          Image: !Join
            - ':'
            - - !Ref Image
              - !Ref ContainerTag
          EntryPoint: 
            - /entrypoint.sh
          Command: 
            - scheduler 
          WorkingDirectory: /usr/local/airflow
          PortMappings:
            - ContainerPort: 8080
          LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-region: !Ref 'AWS::Region'
              awslogs-group: !Ref LogGroup
              awslogs-stream-prefix: ecs
          Environment:
            - Name: LOAD_EX
              Value: n
            - Name: POSTGRES_HOST 
              Value: !GetAtt pgDB.Endpoint.Address 
            - Name: POSTGRES_USER
              Value: !Join ['', ['{{resolve:secretsmanager:', !Ref MyRDSInstanceSecret, ':SecretString:username}}' ]]
            - Name: POSTGRES_PASSWORD
              Value: !Join ['', ['{{resolve:secretsmanager:', !Ref MyRDSInstanceSecret, ':SecretString:password}}' ]]
            - Name: POSTGRES_DB
              Value: airflow
            - Name: FERNET_KEY
              Value: !Join ['', ['{{resolve:secretsmanager:', !Ref AirflowFernetKeySecretName, ':SecretString:key}}' ]]
            - Name: EXECUTOR
              Value: Celery
            - Name: REDIS_HOST 
              Value: redis.ecslocal 
            - Name: AIRFLOW__CORE__REMOTE_BASE_LOG_FOLDER
              Value: !Join ['.',['s3://ca.rogers.vpm', !Ref Stage , 'logs/airflow/logs']]
            - Name: AIRFLOW__CORE__REMOTE_LOGGING
              Value: True
            - Name: AIRFLOW__CORE__REMOTE_LOG_CONN_ID
              Value: aws_default
            - Name: AIRFLOW__WEBSERVER__BASE_URL
              Value: !Join ['',['https://rogers-vpm-airflow-', !Ref Stage, '.massiveinsights.com']]

  WorkerTaskDefinition:
    Type: 'AWS::ECS::TaskDefinition'
    Properties:
      Family: !Join 
        - '-'
        - - !Ref AppName
          - worker-taskdef
      NetworkMode: awsvpc
      RequiresCompatibilities:
        - FARGATE
      Cpu: 2048 
      Memory: 4GB
      ExecutionRoleArn: !Ref ExecutionRole
      TaskRoleArn: !Ref TaskRole
      ContainerDefinitions:
        - Name: worker-cont
          Image: !Join
            - ':'
            - - !Ref Image
              - !Ref ContainerTag
          EntryPoint: 
            - /entrypoint.sh
          Command: 
            - worker 
          WorkingDirectory: /usr/local/airflow
          PortMappings:
            - ContainerPort: 8080
          LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-region: !Ref 'AWS::Region'
              awslogs-group: !Ref LogGroup
              awslogs-stream-prefix: ecs
          Environment:
            - Name: LOAD_EX
              Value: n
            - Name: POSTGRES_HOST 
              Value: !GetAtt pgDB.Endpoint.Address 
            - Name: POSTGRES_USER
              Value: !Join ['', ['{{resolve:secretsmanager:', !Ref MyRDSInstanceSecret, ':SecretString:username}}' ]]
            - Name: POSTGRES_PASSWORD
              Value: !Join ['', ['{{resolve:secretsmanager:', !Ref MyRDSInstanceSecret, ':SecretString:password}}' ]]
            - Name: POSTGRES_DB
              Value: airflow
            - Name: FERNET_KEY
              Value: !Join ['', ['{{resolve:secretsmanager:', !Ref AirflowFernetKeySecretName, ':SecretString:key}}' ]]
            - Name: EXECUTOR
              Value: Celery
            - Name: REDIS_HOST 
              Value: redis.ecslocal 
            - Name: AIRFLOW__CORE__REMOTE_BASE_LOG_FOLDER
              Value: !Join ['.',['s3://ca.rogers.vpm', !Ref Stage , 'logs/airflow/logs']]
            - Name: AIRFLOW__CORE__REMOTE_LOGGING
              Value: True
            - Name: AIRFLOW__CORE__REMOTE_LOG_CONN_ID
              Value: aws_default
            - Name: AIRFLOW__WEBSERVER__BASE_URL
              Value: !Join ['',['https://rogers-vpm-airflow-', !Ref Stage, '.massiveinsights.com']]

  RedisTaskDefinition:
    Type: 'AWS::ECS::TaskDefinition'
    Properties:
      Family: redis 
      NetworkMode: awsvpc
      RequiresCompatibilities:
        - FARGATE
      Cpu: 256
      Memory: 0.5GB
      ExecutionRoleArn: !Ref ExecutionRole
      TaskRoleArn: !Ref TaskRole
      ContainerDefinitions:
        - Name: redis 
          Image: redis:3.2.7 
          PortMappings:
            - ContainerPort: 6379 
          LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-region: !Ref 'AWS::Region'
              awslogs-group: !Ref LogGroup
              awslogs-stream-prefix: ecs

  #######################################################################
  #
  # SERVICES
  #
  #######################################################################
  WebService:
    Type: 'AWS::ECS::Service'
    DependsOn:
      - Listener
    Properties:
      ServiceName: web
      ServiceRegistries:
        - RegistryArn: !GetAtt WebServiceDiscovery.Arn
      Cluster: !Ref Cluster
      TaskDefinition: !Ref WebTaskDefinition
      DeploymentConfiguration:
        MinimumHealthyPercent: 100
        MaximumPercent: 200
      DesiredCount: 1
      HealthCheckGracePeriodSeconds: 30
      LaunchType: FARGATE
      NetworkConfiguration:
        AwsvpcConfiguration:
          AssignPublicIp: ENABLED
          Subnets:
            - !Ref SubnetA
            - !Ref SubnetB
          SecurityGroups:
            - !Ref ContainerSecurityGroup
      LoadBalancers:
        - ContainerName: web-cont
          ContainerPort: 8080
          TargetGroupArn: !Ref TargetGroup

  FlowerService:
    Type: 'AWS::ECS::Service'
    DependsOn:
      - Listener
    Properties:
      ServiceName: flower 
      ServiceRegistries:
        - RegistryArn: !GetAtt FlowerServiceDiscovery.Arn
      Cluster: !Ref Cluster
      TaskDefinition: !Ref FlowerTaskDefinition
      DeploymentConfiguration:
        MinimumHealthyPercent: 100
        MaximumPercent: 200
      DesiredCount: 1
      HealthCheckGracePeriodSeconds: 30
      LaunchType: FARGATE
      NetworkConfiguration:
        AwsvpcConfiguration:
          AssignPublicIp: ENABLED
          Subnets:
            - !Ref SubnetA
            - !Ref SubnetB
          SecurityGroups:
            - !Ref ContainerSecurityGroup
      LoadBalancers:
        - ContainerName: flower-cont
          ContainerPort: 5555
          TargetGroupArn: !Ref FlowerTargetGroup

  SchedulerService:
    Type: 'AWS::ECS::Service'
    Properties:
      ServiceName: scheduler 
      ServiceRegistries:
        - RegistryArn: !GetAtt SchedulerServiceDiscovery.Arn
      Cluster: !Ref Cluster
      TaskDefinition: !Ref SchedulerTaskDefinition
      DeploymentConfiguration:
        MinimumHealthyPercent: 100
        MaximumPercent: 200
      DesiredCount: 1
      LaunchType: FARGATE
      NetworkConfiguration:
        AwsvpcConfiguration:
          AssignPublicIp: ENABLED
          Subnets:
            - !Ref SubnetA
            - !Ref SubnetB
          SecurityGroups:
            - !Ref ContainerSecurityGroup

  WorkerService:
    Type: 'AWS::ECS::Service'
    Properties:
      ServiceName: worker 
      ServiceRegistries:
        - RegistryArn: !GetAtt WorkerServiceDiscovery.Arn
      Cluster: !Ref Cluster
      TaskDefinition: !Ref WorkerTaskDefinition
      DeploymentConfiguration:
        MinimumHealthyPercent: 100
        MaximumPercent: 200
      DesiredCount: 2
      LaunchType: FARGATE
      NetworkConfiguration:
        AwsvpcConfiguration:
          AssignPublicIp: ENABLED
          Subnets:
            - !Ref SubnetA
            - !Ref SubnetB
          SecurityGroups:
            - !Ref ContainerSecurityGroup

  RedisService:
    Type: 'AWS::ECS::Service'
    DependsOn:
      - Listener
    Properties:
      ServiceName: redis
      ServiceRegistries:
        - RegistryArn: !GetAtt RedisServiceDiscovery.Arn
      Cluster: !Ref Cluster
      TaskDefinition: !Ref RedisTaskDefinition
      DeploymentConfiguration:
        MinimumHealthyPercent: 100
        MaximumPercent: 200
      DesiredCount: 1
      LaunchType: FARGATE
      NetworkConfiguration:
        AwsvpcConfiguration:
          AssignPublicIp: ENABLED 
          Subnets:
            - !Ref SubnetA
            - !Ref SubnetB
          SecurityGroups:
            - !Ref ContainerSecurityGroup


############################################################################################
#
# Log Groups
#
############################################################################################
  LogGroup:
    Type: 'AWS::Logs::LogGroup'
    #DeletionPolicy: Retain
    Properties:
      LogGroupName: !Join 
        - ''
        - - !Ref LogsPrefix
          - /ecs/
          - !Ref AppName



############################################################################################
#
# Load Balancer
#
############################################################################################
  TargetGroup:
    Type: 'AWS::ElasticLoadBalancingV2::TargetGroup'
    Properties:
      HealthCheckIntervalSeconds: 10
      HealthCheckPath: /health
      HealthCheckTimeoutSeconds: 5
      UnhealthyThresholdCount: 2
      HealthyThresholdCount: 2
      Name: WebTargetGroup
      Port: 8080
      Protocol: HTTP
      TargetGroupAttributes:
        - Key: deregistration_delay.timeout_seconds
          Value: 60
      TargetType: ip
      VpcId: !Ref VPC

  FlowerTargetGroup:
    Type: 'AWS::ElasticLoadBalancingV2::TargetGroup'
    Properties:
      HealthCheckIntervalSeconds: 60
      HealthCheckPath: /
      HealthCheckTimeoutSeconds: 5
      UnhealthyThresholdCount: 10
      HealthyThresholdCount: 2
      Name: FlowerTargetGroup
      Port: 5555
      Protocol: HTTP
      TargetGroupAttributes:
        - Key: deregistration_delay.timeout_seconds
          Value: 60
      TargetType: ip
      VpcId: !Ref VPC

  Listener:
    Type: 'AWS::ElasticLoadBalancingV2::Listener'
    Properties:
      DefaultActions:
        - TargetGroupArn: !Ref TargetGroup
          Type: forward
      Port: 443
      Protocol: HTTPS
      Certificates: 
        - CertificateArn: !Ref Certificate
      LoadBalancerArn: !Ref LoadBalancer

  HttpRedirectListener:
    Type: AWS::ElasticLoadBalancingV2::Listener
    Properties:
      DefaultActions:
        - RedirectConfig:
            Host: "#{host}"
            Path: "/#{path}"
            Port: 443
            Protocol: "HTTPS"
            Query: "#{query}"
            StatusCode: HTTP_301
          Type: redirect
      LoadBalancerArn: !Ref LoadBalancer
      Port: 80
      Protocol: HTTP

  FlowerListenerRule:
    Type: AWS::ElasticLoadBalancingV2::ListenerRule
    Properties:
      Actions:
      - Type: forward
        TargetGroupArn:
          Ref: FlowerTargetGroup
      Conditions:
      - Field: path-pattern
        Values:
        - "/flower/*"
      ListenerArn:
        Ref: Listener
      Priority: 1

  LoadBalancer:
    Type: 'AWS::ElasticLoadBalancingV2::LoadBalancer'
    Properties:
      LoadBalancerAttributes:
        - Key: idle_timeout.timeout_seconds
          Value: 60
      Name: VPM-WebLoadBalancer
      Scheme: internet-facing
      SecurityGroups:
        - !Ref LoadBalancerSecurityGroup
      Subnets:
        - !Ref SubnetA
        - !Ref SubnetB

  ##########################################################################################
  #
  # RDS
  #
  ##########################################################################################

  #This is a Secret resource with a randomly generated password in its SecretString JSON.
  MyRDSInstanceSecret:
    Type: AWS::SecretsManager::Secret
    Properties:
      Description: 'This is my airflow rds instance secret'
      GenerateSecretString:
        SecretStringTemplate: '{"username": "airflow_user" }'
        GenerateStringKey: 'password'
        PasswordLength: 16
        ExcludeCharacters: '"@/\'
      Tags:
        -
          Key: project 
          Value: vpm 

  myDBSubnetGroup:
    Type: 'AWS::RDS::DBSubnetGroup'
    Properties:
      DBSubnetGroupDescription: DB Private Subnet
      SubnetIds:
        - !Ref SubnetA 
        - !Ref SubnetB 

  # Database
  pgDB:
    Type: 'AWS::RDS::DBInstance'
    Properties:
      DBName: 'airflow'
      AllocatedStorage: !Ref DBAllocatedStorage
      DBInstanceClass: !Ref DBClass
      Engine: postgres
      MasterUsername: !Join ['', ['{{resolve:secretsmanager:', !Ref MyRDSInstanceSecret, ':SecretString:username}}' ]]
      MasterUserPassword: !Join ['', ['{{resolve:secretsmanager:', !Ref MyRDSInstanceSecret, ':SecretString:password}}' ]]
      DBSubnetGroupName: !Ref myDBSubnetGroup
      #DBParameterGroupName: !Ref myDBParamGroup
      VPCSecurityGroups:
        - !GetAtt 
          - DBSecurityGroup
          - GroupId
      StorageEncrypted: True
      EnableCloudwatchLogsExports:
        - postgresql
        - upgrade
      BackupRetentionPeriod: 7


  # Private Network for Cluster
  ################################################################################
  #
  # Service Discovery
  #
  ################################################################################
  ServiceDiscoveryNamespace:
    Type: AWS::ServiceDiscovery::PrivateDnsNamespace
    Properties:
      Name: ecslocal 
      Vpc: !Ref VPC

  RedisServiceDiscovery:
    Type: AWS::ServiceDiscovery::Service
    Properties:
      Name: redis 
      DnsConfig:
        DnsRecords: [{Type: A, TTL: "60"}]
        NamespaceId: !Ref ServiceDiscoveryNamespace
      HealthCheckCustomConfig:
        FailureThreshold: 1

  WebServiceDiscovery:
    Type: AWS::ServiceDiscovery::Service
    Properties:
      Name: web 
      DnsConfig:
        DnsRecords: [{Type: A, TTL: "60"}]
        NamespaceId: !Ref ServiceDiscoveryNamespace
      HealthCheckCustomConfig:
        FailureThreshold: 1
        
  WorkerServiceDiscovery:
    Type: AWS::ServiceDiscovery::Service
    Properties:
      Name: worker 
      DnsConfig:
        DnsRecords: [{Type: A, TTL: "60"}]
        NamespaceId: !Ref ServiceDiscoveryNamespace
      HealthCheckCustomConfig:
        FailureThreshold: 1

  FlowerServiceDiscovery:
    Type: AWS::ServiceDiscovery::Service
    Properties:
      Name: flower
      DnsConfig:
        DnsRecords: [{Type: A, TTL: "60"}]
        NamespaceId: !Ref ServiceDiscoveryNamespace
      HealthCheckCustomConfig:
        FailureThreshold: 1

  SchedulerServiceDiscovery:
    Type: AWS::ServiceDiscovery::Service
    Properties:
      Name: scheduler 
      DnsConfig:
        DnsRecords: [{Type: A, TTL: "60"}]
        NamespaceId: !Ref ServiceDiscoveryNamespace
      HealthCheckCustomConfig:
        FailureThreshold: 1


Outputs:
  Endpoint:
    Description: Endpoint
    Value: !Ref Listener
